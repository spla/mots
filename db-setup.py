import os
import sys
import psycopg2
from psycopg2 import sql
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

def get_parameter( parameter, file_path ):
    # Check if secrets file exists
    if not os.path.isfile(file_path):
        print("File %s not found, asking."%file_path)
        write_parameter( parameter, file_path )
        #sys.exit(0)

    # Find parameter in file
    with open( file_path ) as f:
        for line in f:
            if line.startswith( parameter ):
                return line.replace(parameter + ":", "").strip()

    # Cannot find parameter, exit
    print(file_path + "  Missing parameter %s "%parameter)
    sys.exit(0)

def write_parameter( parameter, file_path ):
    
    if not os.path.exists('config'):
        os.makedirs('config')
    print("Setting up mots parameters...")
    print("\n")
    mots_db = input("mots db name: ")
    mots_db_user = input("mots db user: ")
  
    with open(file_path, "w") as text_file:

        print("mots_db: {}".format(mots_db), file=text_file)
        print("mots_db_user: {}".format(mots_db_user), file=text_file)

def create_table(db, db_user, table, sql):

    conn = None

    try:

        conn = psycopg2.connect(database = db, user = db_user, password = "", host = "/var/run/postgresql", port = "5432")
        cur = conn.cursor()

        print("Creating table.. "+table)
        # Create the table in PostgreSQL database
        cur.execute(sql)

        conn.commit()

        print("Table "+table+" created!")
        print("\n")

    except (Exception, psycopg2.DatabaseError) as error:

        print(error)

    finally:

        if conn is not None:

            conn.close()

def create_index(db, db_user, table, index, sql_index):

  conn = None
  try:

    conn = psycopg2.connect(database = db, user = db_user, password = "", host = "/var/run/postgresql", port = "5432")
    cur = conn.cursor()

    print(f"Creating index...{index}")
    # Create the table in PostgreSQL database
    cur.execute(sql_index)

    conn.commit()

    print(f"Index {index} created!\n")

  except (Exception, psycopg2.DatabaseError) as error:

    print(error)

  finally:

    if conn is not None:

      conn.close()

###############################################################################
# main

if __name__ == '__main__':

    # Load configuration from config file
    config_filepath = "config/db_config.txt"
    mots_db = get_parameter("mots_db", config_filepath)
    mots_db_user = get_parameter("mots_db_user", config_filepath)

    ############################################################
    # create database
    ############################################################

    conn = None

    try:

        conn = psycopg2.connect(dbname='postgres',
            user=mots_db_user, host='',
            password='')

        conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)

        cur = conn.cursor()

        print(f"Creating database {mots_db}. Please wait...")

        cur.execute(sql.SQL("CREATE DATABASE {}").format(
              sql.Identifier(mots_db))
            )
        print(f"Database {mots_db} created!")

    except (Exception, psycopg2.DatabaseError) as error:

        print(error)

    finally:

        if conn is not None:

            conn.close()

    #############################################################################################

    try:

        conn = None
        conn = psycopg2.connect(database = mots_db, user = mots_db_user, password = "", host = "/var/run/postgresql", port = "5432")

    except (Exception, psycopg2.DatabaseError) as error:

        print(error)
        # Load configuration from config file
        os.remove("db_config.txt")
        print("Exiting. Run db-setup again with right parameters")
        sys.exit(0)

    if conn is not None:

        print("\nmots parameters saved to db-config.txt!\n")

    ############################################################
    # Create needed tables
    ############################################################

    print("Creating table...")

    ########################################

    db = mots_db
    db_user = mots_db_user
    table = "words"
    sql = f"create table {table} (word varchar(5) PRIMARY KEY, played boolean default false, played_at timestamptz)"
    create_table(db, db_user, table, sql)

    table = "todayword"
    sql = f"create table {table} (word varchar(5) PRIMARY KEY, played boolean default false, played_at timestamptz)"
    create_table(db, db_user, table, sql)

    table = "games"
    sql = f"create table {table} (word varchar(5), player varchar(30), attempts int, game varchar(60), word1 varchar(5),"
    sql += " word2 varchar(5), word3 varchar(5), word4 varchar(5), word5 varchar(5), word6 varchar(5), win boolean default false)"
    create_table(db, db_user, table, sql)

    index = "games_pkey"
    sql_index = f'CREATE UNIQUE INDEX {index} ON {table} (word, player)'
    create_index(db, db_user, table, index, sql_index)

    #####################################

    print("Done!")
    print("Now you can run setup.py!")
    print("\n")
