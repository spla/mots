import os
import re
import unidecode
from collections import Counter
from datetime import datetime, date
import time
import random
from mastodon import Mastodon
from mastodon.Mastodon import MastodonMalformedEventError, MastodonNetworkError, MastodonReadTimeout, MastodonAPIError
import psycopg2
from PIL import Image, ImageFont, ImageDraw
import pdb

def cleanhtml(raw_html):
    cleanr = re.compile('<.*?>')
    cleantext = re.sub(cleanr, '', raw_html)
    return cleantext

def matching(player_word, today_word): 

    colour_result = []

    for i in range(len(player_word)):

        if player_word[i] is today_word[i]:

            colour_result.append('g')

        elif player_word[i] in today_word:

            colour_result.append('y')

        else:

            colour_result.append('b')

    return colour_result

def unescape(s):
    s = s.replace("&apos;", "'")
    return s

def get_data(notif):

    notification_id = notif.id

    if notif.type != 'mention':

        print(f'dismissing notification {notification_id}')

        mastodon.notifications_dismiss(notification_id)

        return

    account_id = notif.account.id

    username = notif.account.acct

    status_id = notif.status.id

    text  = notif.status.content

    visibility = notif.status.visibility

    reply, player_word = get_word(text)

    player_word = player_word.upper()

    word_exists = check_word(player_word)

    if reply and len(player_word) == 5 and word_exists:

        today_word = load_today_word(date)

        if today_word == '':

            today_word = pick_random_word()

            save_today_word(today_word, date)

        turns, win = check_turns(username, today_word)

        if turns <= 6 and not win:

            win = False

            if turns == 1:

                toot_text = f"@{username} has iniciat la partida amb '{player_word.upper()}'\n\n"

            elif turns > 1 and turns < 7:

                toot_text = f"@{username} has jugat la paraula '{player_word.upper()}'\n\n"

            colour_result = matching(player_word, today_word)

            count = Counter(colour_result)

            if count['g'] == 5:

                toot_text += "\n** Correcte!\n\n"

                win = True

            letters = save_game(today_word, player_word, username, turns, colour_result, win)

            games, winned, perc = player_data(username)

            graphit(username, player_word, colour_result, turns, games, winned, perc, letters)

            panel_image = f'app/panel/{username}_panel.png'

            image_id = mastodon.media_post(panel_image, "image/png").id

            toot_id = mastodon.status_post(toot_text, in_reply_to_id=status_id,visibility='direct', media_ids={image_id})

            print(f'Replied notification {notification_id}')

            mastodon.notifications_dismiss(notification_id)

        else:

            if not win:

                toot_text = f"@{username} ja no et queden paraules :-('\n"

            else:

                toot_text = f"@{username} ja has encertat la paraula d'avui! :-)'\n"

            mastodon.status_post(toot_text, in_reply_to_id=status_id,visibility='direct')

            print(f'Replied notification {notification_id}')

            mastodon.notifications_dismiss(notification_id)

    elif reply and player_word == 'PANELL':

        toot_text = f"@{username} el teu panell"

        games, winned, perc = player_data(username)

        createpanel(username, games, winned, perc)

        panel_image = f'app/panel/{username}-stats-panel.png'

        image_id = mastodon.media_post(panel_image, "image/png").id

        toot_id = mastodon.status_post(toot_text, in_reply_to_id=status_id,visibility=visibility, media_ids={image_id})

        print(f'Replied notification {notification_id}')

        mastodon.notifications_dismiss(notification_id)

    elif reply and len(player_word) != 5:

        toot_text = f"@{username} la paraula '{player_word}' no té 5 lletres!"

        panel_image = f'app/panel/{username}_panel.png'

        image_id = mastodon.media_post(panel_image, "image/png").id

        mastodon.status_post(toot_text, in_reply_to_id=status_id,visibility='direct', media_ids={image_id})

        print(f'dismissing notification {notification_id}')

        mastodon.notifications_dismiss(notification_id)

    elif reply and not word_exists:

        toot_text = f"@{username} la paraula '{player_word}' no és vàlida!"

        panel_image = f'app/panel/{username}_panel.png'

        image_id = mastodon.media_post(panel_image, "image/png").id

        mastodon.status_post(toot_text, in_reply_to_id=status_id,visibility='direct', media_ids={image_id})

        print(f'dismissing notification {notification_id}')

        mastodon.notifications_dismiss(notification_id)

    else:

        print(f'dismissing notification {notification_id}')

        mastodon.notifications_dismiss(notification_id)

def createpanel(username, games, winned, perc):

    horiz = 10

    vert = 10

    fons = Image.open('app/panel/fons.jpg')

    handle = username

    if '@' in username:

        username, hostname = handle.split('@')

    else:

        hostname = mastodon_hostname 

    panel_title_str = f'{username}'

    # add game icon
    icon_path = 'app/panel/mots.png'
    icon_img = Image.open(icon_path)
    icon_img = icon_img.resize((60,60), Image.ANTIALIAS)

    fons.paste(icon_img, (horiz, vert+5), icon_img)

    logo_img = Image.open('app/panel/logo.png')
    fons.paste(logo_img, (15, 320), logo_img)

    board_img = Image.open('app/panel/tauler-mots.png').convert("RGBA")
    fons.paste(board_img, (345, 20), board_img)

    fons.save(f'app/panel/{handle}-stats-panel.png',"PNG")

    base = Image.open(f'app/panel/{handle}-stats-panel.png').convert('RGBA')
    txt = Image.new('RGBA', base.size, (255,255,255,0))
    fnt = ImageFont.truetype('app/fonts/DroidSans.ttf', 40, layout_engine=ImageFont.LAYOUT_BASIC)
    # get a drawing context
    draw = ImageDraw.Draw(txt)

    draw.text((horiz+70,vert+10), 'Mots', font=fnt, fill=(255,255,255,220)) #fill=(255,255,255,255)) ## full opacity

    fnt = ImageFont.truetype('app/fonts/DroidSans.ttf', 20, layout_engine=ImageFont.LAYOUT_BASIC)

    draw.text((horiz+10,vert+70), 'jugador: ' + panel_title_str, font=fnt, fill=(255,255,255,220)) #fill=(255,255,255,255)) ## full opacity

    if hostname != None:

        draw.text((horiz+10,vert+100), 'des de : ' + hostname, font=fnt, fill=(255,255,255,220)) #fill=(255,255,255,255)) ## full opacity

    draw.text((horiz+10,vert+150), 'Jugades : ' + str(games), font=fnt, fill=(255,255,255,220)) #fill=(255,255,255,255)) ## full opacity

    draw.text((horiz+10,vert+180), 'Encerts : ' + str(winned), font=fnt, fill=(255,255,255,220)) #fill=(255,255,255,255)) ## full opacity

    draw.text((horiz+10,vert+210), '% Éxits : ' + str(perc), font=fnt, fill=(255,255,255,220)) #fill=(255,255,255,255)) ## full opacity

    fnt = ImageFont.truetype('app/fonts/DroidSans.ttf', 35, layout_engine=ImageFont.LAYOUT_BASIC)

    fnt = ImageFont.truetype('app/fonts/DroidSans.ttf', 15, layout_engine=ImageFont.LAYOUT_BASIC)

    draw.text((70,330), f'mots@{mastodon_hostname} - 2022', font=fnt, fill=(255,255,255,200)) #fill=(255,255,255,255)) ## full opacity

    out = Image.alpha_composite(base, txt)

    out.save(f'app/panel/{handle}-stats-panel.png')

def graphit(username, player_word, colour_result, turns, games, winned, perc, letters):

    horiz = 10
    vert = 10

    sq_hor = 10
    sq_ver = 10

    if turns == 1:

        w_pos = [[vert+378,horiz+40],[vert+428,horiz+40],[vert+475,horiz+40],[vert+523,horiz+40],[vert+570,horiz+40]]

        sq_pos = [[sq_ver+367,sq_hor+37],[sq_ver+415,sq_hor+37],[sq_ver+462,sq_hor+37],[sq_ver+510,sq_hor+37],[sq_ver+558,sq_hor+37]]

        fons = Image.open('app/panel/fons.jpg')

        handle = username

        if '@' in username:

            username, hostname = handle.split('@')

        else:

            hostname = mastodon_hostname 

        panel_title_str = f'{username}'

        # add game icon
        icon_path = 'app/panel/mots.png'
        icon_img = Image.open(icon_path)
        icon_img = icon_img.resize((60,60), Image.ANTIALIAS)

        fons.paste(icon_img, (horiz, vert+5), icon_img)

        logo_img = Image.open('app/panel/logo.png')
        fons.paste(logo_img, (15, 320), logo_img)

        board_img = Image.open('app/panel/tauler-mots.png').convert("RGBA")
        fons.paste(board_img, (345, 20), board_img)

        fons.save('app/panel/panel.png',"PNG")

        base = Image.open('app/panel/panel.png').convert('RGBA')
        txt = Image.new('RGBA', base.size, (255,255,255,0))
        fnt = ImageFont.truetype('app/fonts/DroidSans.ttf', 40, layout_engine=ImageFont.LAYOUT_BASIC)
        # get a drawing context
        draw = ImageDraw.Draw(txt)

        draw.text((horiz+70,vert+10), 'Mots', font=fnt, fill=(255,255,255,220)) #fill=(255,255,255,255)) ## full opacity

        fnt = ImageFont.truetype('app/fonts/DroidSans.ttf', 20, layout_engine=ImageFont.LAYOUT_BASIC)
        
        draw.text((horiz+10,vert+70), 'jugador: ' + panel_title_str, font=fnt, fill=(255,255,255,220)) #fill=(255,255,255,255)) ## full opacity

        if hostname != None:

            draw.text((horiz+10,vert+100), 'des de : ' + hostname, font=fnt, fill=(255,255,255,220)) #fill=(255,255,255,255)) ## full opacity
        
        draw.text((horiz+10,vert+150), 'Jugades : ' + str(games), font=fnt, fill=(255,255,255,220)) #fill=(255,255,255,255)) ## full opacity
        
        draw.text((horiz+10,vert+180), 'Encerts : ' + str(winned), font=fnt, fill=(255,255,255,220)) #fill=(255,255,255,255)) ## full opacity
        
        draw.text((horiz+10,vert+210), '% Éxits : ' + str(perc), font=fnt, fill=(255,255,255,220)) #fill=(255,255,255,255)) ## full opacity

        draw.text((horiz+10,vert+250), f'Lletres ({len(letters)}):', font=fnt, fill=(255,255,255,220)) #fill=(255,255,255,255)) ## full opacity

        draw.text((horiz+30,vert+280), f'{str(letters)}', font=fnt, fill=(255,255,255,220)) #fill=(255,255,255,255)) ## full opacity

        fnt = ImageFont.truetype('app/fonts/DroidSans.ttf', 35, layout_engine=ImageFont.LAYOUT_BASIC)

        i = 0

        for letter in player_word:

            if colour_result[i] == 'y':

                yellow_img = Image.open('app/panel/yellow.png')

                base.paste(yellow_img, (sq_pos[i]), yellow_img)

            elif colour_result[i] == 'g':

                green_img = Image.open('app/panel/green.png')

                base.paste(green_img, (sq_pos[i]), green_img)

            draw.text((w_pos[i]), letter.upper(), font=fnt, fill=(255,255,255,220)) #fill=(255,255,255,255)) ## full opacity

            i += 1

        fnt = ImageFont.truetype('app/fonts/DroidSans.ttf', 15, layout_engine=ImageFont.LAYOUT_BASIC)

        draw.text((70,330), f'mots@{mastodon_hostname} - 2022', font=fnt, fill=(255,255,255,200)) #fill=(255,255,255,255)) ## full opacity

    else: 

        if turns == 2:

            horiz = horiz + 48
            sq_hor = sq_hor + 48

        elif turns == 3:

            horiz = horiz + 96
            sq_hor = sq_hor + 96

        elif turns == 4:

            horiz = horiz + 144
            sq_hor = sq_hor + 144

        elif turns == 5:

            horiz = horiz + 192
            sq_hor = sq_hor + 192

        elif turns == 6:

            horiz = horiz + 240
            sq_hor = sq_hor + 240
         
        handle = username

        if '@' in username:

            username, hostname = handle.split('@')

        else:

            hostname = mastodon_hostname 

        sq_pos = [[sq_ver+367,sq_hor+37],[sq_ver+415,sq_hor+37],[sq_ver+463,sq_hor+37],[sq_ver+510,sq_hor+37],[sq_ver+557,sq_hor+37]]

        base = Image.open(f'app/panel/{handle}_panel.png').convert('RGBA')

        txt = Image.new('RGBA', base.size, (255,255,255,0))
        
        # get a drawing context
        draw = ImageDraw.Draw(txt)

        fnt = ImageFont.truetype('app/fonts/DroidSans.ttf', 35, layout_engine=ImageFont.LAYOUT_BASIC)

        i = 0

        for letter in player_word:

            old_vert = vert

            if letter == 'i' or letter == 'j':

                vert = vert + 10

            w_pos = [[vert+380,horiz+40],[vert+428,horiz+40],[vert+474,horiz+40],[vert+523,horiz+40],[vert+570,horiz+40]]

            if colour_result[i] == 'y':

                yellow_img = Image.open('app/panel/yellow.png')

                base.paste(yellow_img, (sq_pos[i]), yellow_img)

            elif colour_result[i] == 'g':

                green_img = Image.open('app/panel/green.png')

                base.paste(green_img, (sq_pos[i]), green_img)

            draw.text((w_pos[i]), letter.upper(), font=fnt, fill=(255,255,255,220)) #fill=(255,255,255,255)) ## full opacity

            vert = old_vert

            i += 1

        score_img = Image.open('app/panel/marcador.png').convert("RGBA")

        base.paste(score_img, (20, 142), score_img)

        fnt = ImageFont.truetype('app/fonts/DroidSans.ttf', 20, layout_engine=ImageFont.LAYOUT_BASIC)
        
        draw.text((20,160), 'Jugades : ' + str(games), font=fnt, fill=(255,255,255,220)) #fill=(255,255,255,255)) ## full opacity
        
        draw.text((20,190), 'Encerts : ' + str(winned), font=fnt, fill=(255,255,255,220)) #fill=(255,255,255,255)) ## full opacity
        
        draw.text((20,220), '% Éxits : ' + str(perc), font=fnt, fill=(255,255,255,220)) #fill=(255,255,255,255)) ## full opacity

        draw.text((20,260), f'Lletres ({len(letters)}):', font=fnt, fill=(255,255,255,220)) #fill=(255,255,255,255)) ## full opacity

        draw.text((40,290), f'{str(letters)}', font=fnt, fill=(255,255,255,220)) #fill=(255,255,255,255)) ## full opacity

    out = Image.alpha_composite(base, txt)

    out.save(f'app/panel/{handle}_panel.png')

def check_turns(username, today_word):

    win = False

    turns = 1

    conn = None

    try:

        conn = psycopg2.connect(database=words_db, user=words_db_user, password="", host="/var/run/postgresql", port="5432")

        cur = conn.cursor()

        cur.execute('select attempts, win from games where player=(%s) and word=(%s)', (username, today_word))

        row = cur.fetchone()

        if row != None:

            turns = turns + row[0]

            win = row[1]

        cur.close()

    except (Exception, psycopg2.DatabaseError) as error:

        print(error)

    finally:

        if conn is not None:

            conn.close()

    return (turns, win)

def player_data(username):

    games = 0

    winned = 0

    conn = None

    try:

        conn = psycopg2.connect(database=words_db, user=words_db_user, password="", host="/var/run/postgresql", port="5432")

        cur = conn.cursor()

        cur.execute('select count(*) from games where player=(%s)', (username,))

        row = cur.fetchone()

        if row != None:

            games = row[0]

        cur.execute('select count(*) from games where player=(%s) and win', (username,))

        row = cur.fetchone()

        if row != None:

            winned = row[0]

        cur.close()

    except (Exception, psycopg2.DatabaseError) as error:

        print(error)

    finally:

        if conn is not None:

            conn.close()

    if games != 0 and winned != 0:
    
        perc = round(((winned * 100) / games), 2)

    else:

        perc = 0

    return (games, winned, perc)

def save_game(today_word, played_word, username, turns, turn_result, win):

    sql_select_game = "select game, letters from games where word=(%s) and player=(%s)"

    sql_insert = f"insert into games (word, player, attempts, game, word{turns}, win, letters) "
    sql_insert += f"values (%s,%s,%s,%s,%s,%s,%s) ON CONFLICT (word, player) "
    sql_insert += f"DO UPDATE SET attempts = EXCLUDED.attempts, game = EXCLUDED.game, word{turns} = EXCLUDED.word{turns}, win = EXCLUDED.win, letters = EXCLUDED.letters"

    conn = None

    try:

        conn = psycopg2.connect(database=words_db, user=words_db_user, password="", host="/var/run/postgresql", port="5432")

        cur = conn.cursor()

        cur.execute(sql_select_game, (today_word, username))

        row = cur.fetchone()

        if row != None:

            turn_result = "/".join(turn_result)

            game = row[0] + "*" + turn_result

            letters = row[1]

        else:

            game = "/".join(turn_result)

            letters = 'ABCDEFGHIJLMNOPQRSTUVXZ'

        for letter in played_word:

            letters = letters.replace(letter,'')

        cur.execute(sql_insert, (today_word, username, turns, game, played_word, win, letters))

        conn.commit()

        cur.close()

    except (Exception, psycopg2.DatabaseError) as error:

        print(error)

    finally:

        if conn is not None:

            conn.close()

    return letters

def get_word(text):

    reply = False

    player_word = ''

    content = cleanhtml(text)
    content = unescape(content)

    try:

        start = content.index("@")
        end = content.index(" ")
        if len(content) > end:

            content = content[0: start:] + content[end +1::]

        cleanit = content.count('@')

        i = 0
        while i < cleanit :

            start = content.rfind("@")
            end = len(content)
            content = content[0: start:] + content[end +1::]
            i += 1

        content = content.lower()
        player_word = content

        if unidecode.unidecode(player_word)[0:3] == 'mot':

            reply = True

            start = 0
            end = unidecode.unidecode(player_word).index('mot', 0)
            if len(player_word) > end:
                player_word = player_word[0: start:] + player_word[end + len('mot')+1::]

        if unidecode.unidecode(player_word)[0:6] == 'panell':

            reply = True

    except:

        pass

    return (reply, player_word)

def check_word(word):

    word_exists = False

    conn = None

    try:

        conn = psycopg2.connect(database=words_db, user=words_db_user, password="", host="/var/run/postgresql", port="5432")

        cur = conn.cursor()

        cur.execute('select word from words where word=(%s)', (word,))

        row = cur.fetchone()

        if row != None:

            word_exists = True

        cur.close()

    except (Exception, psycopg2.DatabaseError) as error:

        print(error)

    finally:

        if conn is not None:

            conn.close()

    return word_exists

def load_today_word(date):

    todayword = ''

    conn = None

    try:

        conn = psycopg2.connect(database=words_db, user=words_db_user, password="", host="/var/run/postgresql", port="5432")

        cur = conn.cursor()

        cur.execute('select word from todayword where played and played_at=(%s)', (date,))

        row = cur.fetchone()

        if row != None:

            todayword = row[0]

        cur.close()

    except (Exception, psycopg2.DatabaseError) as error:

        print(error)

    finally:

        if conn is not None:

            conn.close()

    return todayword

def pick_random_word():

    words_list = []

    conn = None

    try:

        conn = psycopg2.connect(database=words_db, user=words_db_user, password="", host="/var/run/postgresql", port="5432")

        cur = conn.cursor()

        cur.execute('select word from words where not played')

        rows = cur.fetchall()

        for row in rows:

            words_list.append(row)

        cur.close()

    except (Exception, psycopg2.DatabaseError) as error:

        print(error)

    finally:

        if conn is not None:

            conn.close()

    today_word = random.choice(words_list)[0]

    return today_word

def save_today_word(word, date):

    played = True

    insert_today_sql = "INSERT INTO todayword(word, played, played_at) VALUES(%s,%s,%s) ON CONFLICT DO NOTHING"

    update_word_sql = "UPDATE words SET played=(%s), played_at=(%s) where word=(%s)"

    conn = None

    try:

        conn = psycopg2.connect(database=words_db, user=words_db_user, password="", host="/var/run/postgresql", port="5432")

        cur = conn.cursor()

        cur.execute(insert_today_sql, (word, played, date))

        cur.execute(update_word_sql, ('t', date, word))

        conn.commit()

        cur.close()

    except (Exception, psycopg2.DatabaseError) as error:

        print(error)

    finally:

        if conn is not None:

            conn.close()

def log_in():

    # Load secrets from secrets file
    secrets_filepath = "secrets/secrets.txt"
    uc_client_id     = get_parameter("uc_client_id",     secrets_filepath)
    uc_client_secret = get_parameter("uc_client_secret", secrets_filepath)
    uc_access_token  = get_parameter("uc_access_token",  secrets_filepath)

    # Load configuration from config file
    config_filepath = "config/config.txt"
    mastodon_hostname = get_parameter("mastodon_hostname", config_filepath)

    # Initialise Mastodon API
    mastodon = Mastodon(
        client_id = uc_client_id,
        client_secret = uc_client_secret,
        access_token = uc_access_token,
        api_base_url = 'https://' + mastodon_hostname,
    )

    # Initialise access headers
    headers={ 'Authorization': 'Bearer %s'%uc_access_token }

    return (mastodon_hostname, mastodon)

def db_config():

    # Load configuration from config file
    config_filepath = "config/db_config.txt"
    words_db = get_parameter("words_db", config_filepath)
    words_db_user = get_parameter("words_db_user", config_filepath)

    return (words_db, words_db_user)

def get_parameter( parameter, file_path ):

    # Check if secrets file exists
    if not os.path.isfile(file_path):
        print("File %s not found, asking."%file_path)
        write_parameter( parameter, file_path )
        #sys.exit(0)

    # Find parameter in file
    with open( file_path ) as f:
        for line in f:
            if line.startswith( parameter ):
                return line.replace(parameter + ":", "").strip()

    # Cannot find parameter, exit
    print(file_path + "  Missing parameter %s "%parameter)
    sys.exit(0)

###############################################################################
# main

if __name__ == '__main__':

    # Thanks to Joan Montané for his Diccionari Informatitzat de l'Scrablle en Català http://escrable.montane.cat/diccionari/

    mastodon_hostname, mastodon = log_in()

    date = date.today()

    words_db, words_db_user = db_config()

    bot_notifications = mastodon.notifications()

    start = time.time()

    for mention in bot_notifications:

        get_data(mention)

    print(f"duration = {time.time() - start}.\nprocessed queries: {len(bot_notifications)}")
    
