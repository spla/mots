# Mots
Mots és un programa fet amb Python que permet a qualsevol usuari del fedivers mirar d'encertat la paraula del dia.  

Com es juga?    

`@mots mot <paraula>`  
  
### Dependències

-   **Python 3**
-   Compte del bot a Mastodon
-   Servidor Postgresql

### Com instal·lar Mots:

1. Clona el repositori: `git clone https://gitlab.com/spla/mots.git <directori>`  

2. Entra amb `cd` al directori `<directori>` i crea l'Entorn Virtual de Python: `python3.x -m venv .`  
 
3. Activa l'Entorn Virtual de Python: `source bin/activate`  
  
4. Executa `pip install -r requirements.txt` per a afegir les llibreries necessàries.  

5. Executa `python setup.py` per a aconseguir els tokens d'accés del compte del bot de  Mastodon.  

6. Fes servir el teu programador de tasques preferit (per exemple crontab) per a que `python mots.py` s'executi cada minut.  

A jugar!
